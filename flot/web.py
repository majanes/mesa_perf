#!/usr/bin/python3
# encoding=utf-8
# Copyright © 2016 Intel Corporation

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Flask webpage for performance data."""

import itertools

# pylint: disable=import-error
import flask
from flask import render_template
# pylint: enable=import-error

APP = flask.Flask(__name__)

# pylint: disable=bad-whitespace
_BENCHMARKS = [
    ('gfxbench5_manhattan',                         'synthetic-benchmark'),
    ('gfxbench5_manhattan_o',                       'synthetic-benchmark'),
    ('gfxbench5_car_chase',                         'synthetic-benchmark'),
    ('gfxbench5_car_chase_o',                       'synthetic-benchmark'),
    ('gfxbench5_tess',                              'synthetic-benchmark'),
    ('gfxbench5_trex',                              'synthetic-benchmark'),
    ('gfxbench5_trex_o',                            'synthetic-benchmark'),
    ('gfxbench5_aztec_ruins_gl_normal',             'synthetic-benchmark'),
    ('gfxbench5_alu2',                              'synthetic-benchmark'),
    ('gfxbench5_fill',                              'synthetic-benchmark'),
    ('gfxbench5_driver2',                           'synthetic-benchmark'),
    ('gfxbench5_egypt',                             'synthetic-benchmark'),
    ('gfxbench5_egypt_o',                           'synthetic-benchmark'),
    ('synmark_OglBatch0',                           'micro-benchmark'),       
    ('synmark_OglBatch1',                           'micro-benchmark'),       
    ('synmark_OglBatch2',                           'micro-benchmark'),       
    ('synmark_OglBatch3',                           'micro-benchmark'),       
    ('synmark_OglBatch4',                           'micro-benchmark'),       
    ('synmark_OglBatch5',                           'micro-benchmark'),       
    ('synmark_OglBatch6',                           'micro-benchmark'),       
    ('synmark_OglBatch7',                           'micro-benchmark'),       
    ('synmark_OglCSCloth',                          'micro-benchmark'),       
    ('synmark_OglCSDof',                            'micro-benchmark'),       
    ('synmark_OglDeferred',                         'micro-benchmark'),       
    ('synmark_OglDeferredAA',                       'micro-benchmark'),       
    ('synmark_OglDrvRes',                           'micro-benchmark'),       
    ('synmark_OglDrvShComp',                        'micro-benchmark'),       
    ('synmark_OglDrvState',                         'micro-benchmark'),       
    ('synmark_OglFillPixel',                        'micro-benchmark'),       
    ('synmark_OglFillTexMulti',                     'micro-benchmark'),       
    ('synmark_OglFillTexSingle',                    'micro-benchmark'),       
    ('synmark_OglGeomPoint',                        'micro-benchmark'),       
    ('synmark_OglGeomTriList',                      'micro-benchmark'),       
    ('synmark_OglGeomTriStrip',                     'micro-benchmark'),       
    ('synmark_OglHdrBloom',                         'micro-benchmark'),       
    ('synmark_OglMultithread',                      'micro-benchmark'),       
    ('synmark_OglPSBump2',                          'micro-benchmark'),       
    ('synmark_OglPSBump8',                          'micro-benchmark'),       
    ('synmark_OglPSPhong',                          'micro-benchmark'),       
    ('synmark_OglPSPom',                            'micro-benchmark'),       
    ('synmark_OglShMapPcf',                         'micro-benchmark'),       
    ('synmark_OglShMapVsm',                         'micro-benchmark'),       
    ('synmark_OglTerrainFlyInst',                   'micro-benchmark'),       
    ('synmark_OglTerrainFlyTess',                   'micro-benchmark'),       
    ('synmark_OglTerrainPanInst',                   'micro-benchmark'),       
    ('synmark_OglTerrainPanTess',                   'micro-benchmark'),       
    ('synmark_OglTexFilterAniso',                   'micro-benchmark'),       
    ('synmark_OglTexFilterTri',                     'micro-benchmark'),       
    ('synmark_OglTexMem128',                        'micro-benchmark'),       
    ('synmark_OglTexMem512',                        'micro-benchmark'),       
    ('synmark_OglVSDiffuse1',                       'micro-benchmark'),       
    ('synmark_OglVSDiffuse8',                       'micro-benchmark'),       
    ('synmark_OglVSInstancing',                     'micro-benchmark'),       
    ('synmark_OglVSTangent',                        'micro-benchmark'),       
    ('synmark_OglZBuffer',                          'micro-benchmark'),       
    ('unigine_heaven',                              'engine-demo'),           
    ('unigine_valley',                              'engine-demo'),           
    ('xonotic_ultra',                               'game-demo'),
]
# pylint: enable=bad-whitespace


class _Getter(object):
    """A container for making working with benchmark data easier.

    Stores dictionaries relating each element to each other, allowing for fast
    searches.

    """
    def __init__(self):
        self.by_name = dict(iter(_BENCHMARKS))
        self.by_category = {c: [n[0] for n in b] for c, b in itertools.groupby(
            sorted(_BENCHMARKS, key=lambda x: x[1]), lambda x: x[1])}


GETTER = _Getter()


@APP.route('/')
def front():
    return render_template('index.html', getter=GETTER)


@APP.route('/apps/all')
def all():  # pylint: disable=redefined-builtin
    return render_template('apps.html', benchmarks=dict(_BENCHMARKS),
                           category="All Benchmarks")


@APP.route('/apps/<benchmark>')
def apps(benchmark):
    return render_template(
        'apps.html',
        benchmarks=[benchmark],
        category=None)


@APP.route('/categories/<category>')
def categories(category):
    return render_template(
        'apps.html',
        benchmarks=GETTER.by_category[category],
        category=category)


if __name__ == '__main__':
    APP.run()
