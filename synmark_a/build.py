#!/usr/bin/python

import sys
import os.path as path
sys.path.append(path.join(path.dirname(path.abspath(sys.argv[0])), "..", "repos", "mesa_ci"))
import build_support as bs

class SynmarkTimeout:
    def __init__(self):
        self._options = bs.Options()
    def GetDuration(self):
        limit = 30
        if self._options.type == "daily":
            limit *= 8
        if "bsw" in self._options.hardware:
            limit *= 2
        return limit

def iterations(bench, hw):
    if bench == "synmark.OglBatch5":
        if hw == "skl":
            return 4
    if bench == "synmark.OglBatch6":
        if hw == "bdw":
            return 7
        return 4
    if bench == "synmark.OglBatch7":
        return 4
    if bench == "synmark.OglDrvRes":
        if hw == "skl":
            return 3
    if bench == "synmark.OglDrvState":
        return 4
    if bench == "synmark.OglTerrainFlyInst":
        if hw == "skl":
            return 3
    if bench == "synmark.OglZBuffer":
        if hw == "bdw":
            return 3
    if bench == "synmark.OglTerrainFlyTess":
        if hw == "bdw":
            return 4
    
    
high_variance_benchmarks = ["synmark.OglVSInstancing",
                            "synmark.OglTerrainFlyInst",
                            "synmark.OglMultithread",
                            "synmark.OglBatch7",
                            "synmark.OglDrvRes",
                            "synmark.OglDrvShComp",
                            "synmark.OglCSDof",
                            "synmark.OglBatch6",
                            "synmark.OglTerrainFlyTess",
                            "synmark.OglDrvState",
                            "synmark.OglBatch5",
                            "synmark.OglTerrainPanInst",
                            "synmark.OglZBuffer",
                            "synmark.OglBatch2"]

bs.build(bs.PerfBuilder(high_variance_benchmarks, iterations=2,
                        custom_iterations_fn=iterations),
         time_limit=SynmarkTimeout())

