 #!/usr/bin/env python3

import os
import sys
import os.path as path
sys.path.append(path.join(path.dirname(path.abspath(sys.argv[0])), "..", "repos", "mesa_ci"))
import build_support as bs


class MesaPerf:
    def __init__(self):
        self.opts = bs.Options()

        if self.opts.config == 'debug':
            print("ERROR: perf not supported for debug")
            assert(False)

        pm = bs.ProjectMap()
        self._src_dir = pm.project_source_dir("mesa")
        self._build_dir = self._src_dir + "/" + '_'.join(['build', 'kbl'])
        self._install_dir = ("/tmp/build_root/" + self.opts.arch
                             + "/kbl/usr/local")

        # generated with `gcc -E -v -march=native - < /dev/null 2>&1 | grep cc1`
        # gcc has no -march=native flags for kbl at this point
        self._flags = ['-march=skylake', '-mmmx',
                       '-mno-3dnow', '-msse', '-msse2',
                       '-msse3', '-mssse3', '-mno-sse4a',
                       '-mcx16', '-msahf', '-mmovbe',
                       '-maes', '-mno-sha', '-mpclmul',
                       '-mpopcnt', '-mabm', '-mno-lwp',
                       '-mfma', '-mno-fma4', '-mno-xop',
                       '-mbmi', '-mbmi2', '-mno-tbm',
                       '-mavx', '-mavx2', '-msse4.2',
                       '-msse4.1', '-mlzcnt', '-mno-rtm',
                       '-mno-hle', '-mrdrnd', '-mf16c',
                       '-mfsgsbase', '-mrdseed', '-mprfchw',
                       '-madx', '-mfxsr', '-mxsave',
                       '-mxsaveopt', '-mno-avx512f',
                       '-mno-avx512er', '-mno-avx512cd',
                       '-mno-avx512pf', '-mno-prefetchwt1',
                       '-mclflushopt', '-mxsavec',
                       '-mxsaves', '-mno-avx512dq',
                       '-mno-avx512bw', '-mno-avx512vl',
                       '-mno-avx512ifma', '-mno-avx512vbmi',
                       '-mno-clwb', '-mno-mwaitx',
                       '-mno-clzero', '-mno-pku', '--param',
                       'l1-cache-size=32', '--param',
                       'l1-cache-line-size=64', '--param',
                       'l2-cache-size=4096',
                       '-mtune=skylake']
        self._flags = " ".join(self._flags)

    def build(self):

        flags = " ".join(self._flags)
        flags = ["CFLAGS=-O2 " + flags,
                 "CXXFLAGS=-O2 " + flags,
                 "CC=ccache gcc",
                 "CXX=ccache g++"]

        options = [
            '-Dgallium-drivers=iris',
            '-Ddri-drivers=i965,i915',
            '-Dvulkan-drivers=intel',
            '-Dplatforms=x11,drm',
            '-Dbuildtype=release',
            '-Db_ndebug=true',
            '-Dllvm=false'
        ]
        b = bs.builders.MesonBuilder(extra_definitions=options, install=True,
                                     cpp_args=self._flags,
                                     build_dir=self._build_dir,
                                     install_dir=self._install_dir,
                                     project='mesa')
        bs.build(b)

        bs.Export().export_perf()

    def clean(self):
        pm = bs.ProjectMap()
        bs.git_clean(pm.project_source_dir("mesa"))
        bs.rmtree(self._build_dir)

    def test(self):
        pass
       
bs.build(MesaPerf())
