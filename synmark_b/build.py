#!/usr/bin/python

import sys
import os.path as path
sys.path.append(path.join(path.dirname(path.abspath(sys.argv[0])), "..", "repos", "mesa_ci"))
import build_support as bs

class SynmarkTimeout:
    def __init__(self):
        self._options = bs.Options()
    def GetDuration(self):
        if self._options.type == "daily":
            return 120
        return 30

def iterations(bench, hw):
    if bench == "synmark.OglHdrBloom":
        if hw == "skl":
            return 4
    if bench == "synmark.OglVSTangent":
        if hw == "bdw":
            return 4

high_variance_benchmarks = ["synmark.OglFillTexSingle",
                            "synmark.OglBatch3",
                            "synmark.OglDeferred",
                            "synmark.OglBatch1",
                            "synmark.OglBatch4",
                            "synmark.OglGeomPoint",
                            "synmark.OglBatch0",
                            "synmark.OglHdrBloom",
                            "synmark.OglCSCloth",
                            "synmark.OglShMapVsm",
                            "synmark.OglVSTangent",
                            "synmark.OglFillTexMulti",
                            "synmark.OglFillPixel",
                            "synmark.OglGeomTriList"]

bs.build(bs.PerfBuilder(high_variance_benchmarks, iterations=2,
                        custom_iterations_fn=iterations),
         time_limit=SynmarkTimeout())
