#!/usr/bin/python

import sys
import os.path as path
sys.path.append(path.join(path.dirname(path.abspath(sys.argv[0])), "..", "repos", "mesa_ci"))
import build_support as bs

class SynmarkTimeout:
    def __init__(self):
        self._options = bs.Options()
    def GetDuration(self):
        if self._options.type == "daily":
            return 120
        return 30

def iterations(bench, hw):
    if bench == "synmark.OglTexFilterAniso":
        if hw == "bdw":
            return 4
    if bench == "synmark.OglTexFilterTri":
        if hw == "bdw":
            return 10
    if bench == "synmark.OglTerrainPanTess":
        if hw == "bdw":
            return 5
    if bench == "synmark.OglTexMem128":
        if hw == "bdw":
            return 4
    if bench == "synmark.OglVSDiffuse8":
        if hw == "bdw":
            return 8
    

low_variance_benchmarks = ["synmark.OglPSBump8",
                           "synmark.OglDeferredAA",
                           "synmark.OglTexMem128",
                           "synmark.OglTerrainPanTess",
                           "synmark.OglPSPhong",
                           "synmark.OglVSDiffuse1",
                           "synmark.OglPSPom",
                           "synmark.OglGeomTriStrip",
                           "synmark.OglPSBump2",
                           "synmark.OglTexMem512",
                           "synmark.OglTexFilterAniso",
                           "synmark.OglVSDiffuse8",
                           "synmark.OglTexFilterTri",
                           "synmark.OglShMapPcf"]

bs.build(bs.PerfBuilder(low_variance_benchmarks, iterations=3),
         time_limit=SynmarkTimeout())

